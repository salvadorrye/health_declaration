# Generated by Django 3.1.4 on 2021-06-24 02:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('health_declaration', '0007_auto_20210624_0002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='healthdeclaration',
            name='day_of_quarantine',
        ),
        migrations.AddField(
            model_name='healthdeclaration',
            name='age',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='swabtestresult',
            name='day_of_quarantine',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='swabtestresult',
            name='specimen',
            field=models.CharField(blank=True, max_length=160),
        ),
    ]
