# Generated by Django 3.1.4 on 2021-06-10 06:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('health_declaration', '0004_healthdeclaration_direction_to_work'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='healthdeclaration',
            name='direction_to_work',
        ),
        migrations.AddField(
            model_name='swabtestresult',
            name='direction_to_work',
            field=models.CharField(blank=True, choices=[('i', 'Incoming'), ('o', 'Outgoing')], max_length=1),
        ),
    ]
