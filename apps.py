from django.apps import AppConfig


class HealthDeclarationConfig(AppConfig):
    name = 'health_declaration'
