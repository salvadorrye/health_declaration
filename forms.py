from django import forms
from django.utils.safestring import mark_safe
from .models import HealthDeclaration
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class HealthDeclarationForm(forms.ModelForm):
    traveled_outside = forms.TypedChoiceField(
                   label='Have you traveled to or live in local areas where there are reported cases of COVID-19?',
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    exposed_to_suspected_probable_or_confirmed = forms.TypedChoiceField(
                   label='Have you been exposed to patient under the classification of SUSPECTED or PROBABLE or CONFIRMED case of COVID-19?',
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    seen_for_probability_or_suspicion = forms.TypedChoiceField(
                   label='Were you previously seen for probablity or suspicion of COVID-19?',
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    undergone_testing_or_swabbing = forms.TypedChoiceField(
                   label='Have you undergone COVID-19 Testing or Swabbing?',
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    do_you_authorize_camillus_medhaven = forms.TypedChoiceField(
                   label=mark_safe('Do you authorize St. Camillus MedHaven Nursing Home to collect and process the data indicated herein to ensure the well-being and protection of everyone? <br/> <br/> Do you understand that your personal information is protected by RA 10173, Data Privacy Act of 2012, and that you are required by RA 11469, Bayanihan to Heal as One Act , to provide truthful information?'),
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect,
                   initial='1',
                   required=True
                )
    headache = forms.TypedChoiceField(
                   label=mark_safe('Are you experiencing: <br/> <br/>Headache'),
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    fever = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    cough_and_colds = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    sore_throat = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    shortness_of_breath = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    muscle_pains_or_body_weakness = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    joint_pains = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    diarrhea = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    loss_of_sense_of_smell_and_taste = forms.TypedChoiceField(
                   coerce=lambda x: bool(int(x)),
                   choices=((1, 'Yes'), (0, 'No')),
                   widget=forms.RadioSelect
                )
    class Meta:
        model = HealthDeclaration
        exclude = ['date',]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-exampleForm'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.form_action = 'health-declaration'

        self.helper.add_input(Submit('submit', 'Submit'))
