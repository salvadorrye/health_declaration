from django.db import models
from django.conf import settings
from django.core.validators import RegexValidator
from django.urls import reverse

SEX = (
    ('male', 'Male'),
    ('female', 'Female'),
    )

PERSONNEL_TYPE = (
    ('employee', 'Employee'),
    ('visitor', 'Visitor'),
    )

DIRECTION_TYPE = (
    ('i', 'Incoming'),
    ('o', 'Outgoing'),
)

RESULT = (
    ('-', 'Negative'),
    ('+', 'Positive'),
)

SWAB_TEST = (
    ('0', 'Standard Q Covid-19 Ag'),
    ('1', 'Standard F Covid-19 Ag FIA'),
)

MACHINE = (
    ('0', 'SD Biosensor F100'),
    ('1', 'SD Biosensor F200'),
)



BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))

# Create your models here.
class HealthDeclaration(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    temperature = models.DecimalField(max_digits=3, decimal_places=1)
    personnel_type = models.CharField(max_length=8, choices=PERSONNEL_TYPE, blank=False)
    last_name = models.CharField(max_length=35)
    given_name = models.CharField(max_length=35, verbose_name='name')
    middle_name = models.CharField(max_length=35, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    cellphone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    email = models.EmailField(null=True, blank=True)
    age = models.PositiveSmallIntegerField(null=True, blank=False)
    gender = models.CharField(max_length=6, choices=SEX, blank=False)
    department = models.ForeignKey('Department', related_name='health_declarations', on_delete=models.PROTECT)
    traveled_outside = models.BooleanField(choices=BOOL_CHOICES)
    exposed_to_suspected_probable_or_confirmed = models.BooleanField(choices=BOOL_CHOICES)
    seen_for_probability_or_suspicion = models.BooleanField(choices=BOOL_CHOICES)
    undergone_testing_or_swabbing = models.BooleanField(choices=BOOL_CHOICES)
    do_you_authorize_camillus_medhaven = models.BooleanField(choices=BOOL_CHOICES)
    headache = models.BooleanField(choices=BOOL_CHOICES)
    fever = models.BooleanField(choices=BOOL_CHOICES)
    cough_and_colds = models.BooleanField(choices=BOOL_CHOICES)
    sore_throat = models.BooleanField(choices=BOOL_CHOICES)
    shortness_of_breath = models.BooleanField(choices=BOOL_CHOICES)
    muscle_pains_or_body_weakness = models.BooleanField(choices=BOOL_CHOICES)
    joint_pains = models.BooleanField(choices=BOOL_CHOICES)
    diarrhea = models.BooleanField(choices=BOOL_CHOICES)
    loss_of_sense_of_smell_and_taste = models.BooleanField(choices=BOOL_CHOICES)

    class Meta:
        ordering = ('-date',)

    def __str__(self):
        return f'{self.date.strftime("%m/%d/%Y, %H:%M%p")} - {self.given_name} {self.last_name}'

class Department(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'department'

    def __str__(self):
        return self.name

class SwabTestResult(models.Model):
    patient = models.OneToOneField(HealthDeclaration, on_delete=models.CASCADE, primary_key=True)
    date = models.DateField(auto_now=False, null=False, blank=False)
    direction_to_work = models.CharField(max_length=1, choices=DIRECTION_TYPE, blank=True)
    day_of_quarantine = models.PositiveSmallIntegerField(null=True, blank=True) 
    specimen = models.CharField(max_length=160, blank=True)
    swab_test = models.CharField(max_length=1, choices=SWAB_TEST, blank=True)
    machine = models.CharField(max_length=1, choices=MACHINE, blank=True)
    COI = models.DecimalField(max_digits=3, decimal_places=2, null=True, blank=True)
    test_result = models.CharField(max_length=1, choices=RESULT, blank=False)
    photo = models.ImageField(
            null=True,
            blank=True,
            upload_to="photos/swab_test_results/%Y/%m/%D",
            )
    staff_nurse = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date',]

    def __str__(self):
        return f'{self.date} - {self.patient.last_name}, {self.patient.given_name} {self.patient.middle_name} ({self.get_test_result_display()})'

    def get_absolute_url(self):
        return reverse('swab-detail', args=[str(self.patient.id)])

