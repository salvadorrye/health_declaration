from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import HealthDeclaration, Department, SwabTestResult
from django.utils.html import mark_safe

class HealthDeclarationAdminSite(AdminSite):
    site_header = 'St. Camillus MedHaven Plan of Care Admin'
    site_title = 'St. Camillus MedHaven Plan of Care Admin Portal'
    index_title = 'Welcome to St. Camillus MedHaven Plan of Care Portal'

health_declaration_admin_site = HealthDeclarationAdminSite(name='health_declaration_admin')

# Register your models here.
@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}

@admin.register(HealthDeclaration)
class HealthDeclarationAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ['date', 'temperature', 'personnel_type', 'last_name', 'given_name', 'middle_name', 'gender',
            'cellphone_number', 'email', 'department']
    list_filter = ['date', 'personnel_type', 'gender',
            'traveled_outside',
            'exposed_to_suspected_probable_or_confirmed',
            'seen_for_probability_or_suspicion',
            'undergone_testing_or_swabbing',
            'do_you_authorize_camillus_medhaven',
            'headache', 'fever', 'cough_and_colds', 'sore_throat', 'shortness_of_breath', 'muscle_pains_or_body_weakness',
            'joint_pains', 'diarrhea', 'loss_of_sense_of_smell_and_taste',
            ]
    fieldsets = (
        ('Personal details', {'fields': ('temperature', 'personnel_type', 'last_name', 'given_name', 'middle_name', 
            'gender', 'cellphone_number', 'email', 'department',)}),
        ('Work-related history', {'fields': ('traveled_outside',
                                'exposed_to_suspected_probable_or_confirmed',
                                'seen_for_probability_or_suspicion',
                                'undergone_testing_or_swabbing',
                                'do_you_authorize_camillus_medhaven', 
                                )}),
        ('Are you experiencing:', {'fields': ('headache', 'fever', 'cough_and_colds',
                                              'sore_throat', 'shortness_of_breath',
                                              'muscle_pains_or_body_weakness',
                                              'joint_pains',
                                              'diarrhea',
                                              'loss_of_sense_of_smell_and_taste',)}),
    )

@admin.register(SwabTestResult)
class SwabTestResultAdmin(admin.ModelAdmin):
    list_display = ['date', 'patient', 'direction_to_work', 'day_of_quarantine', 'test_result', 'staff_nurse']
    list_filter = ['date', 'direction_to_work', 'test_result']
    readonly_fields = ['photo_image']
    excluded = ['staff_nurse']

    def save_model(self, request, obj, form, change):
        obj.staff_nurse = request.user
        super().save_model(request, obj, form, change)

    def photo_image(self, obj):
        return mark_safe(f'<img src="{obj.photo.url}" class="img-fluid" />')
    

health_declaration_admin_site.register(Department, admin_class=DepartmentAdmin)
health_declaration_admin_site.register(HealthDeclaration, admin_class=HealthDeclarationAdmin)
health_declaration_admin_site.register(SwabTestResult, admin_class=SwabTestResultAdmin)

