from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.health_declaration, name='health-declaration'),
    path('swab_test_result/<int:pk>', views.SwabTestResultDetailView.as_view(), name='swab-detail'),
]

