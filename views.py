from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import HealthDeclarationForm
from django.views import generic
from .models import SwabTestResult
from django.contrib.auth.mixins import PermissionRequiredMixin

# Create your views here.
def health_declaration(request):
    if request.method == 'POST':
        form = HealthDeclarationForm(request.POST)
        if form.is_valid():
            new_entry = form.save()
            form = HealthDeclarationForm()
            messages.success(request, 'Thank You! Your application form has been submitted successfully.')
        else:
            messages.error(request, 'Error submitting your application form.')
    else:
        form = HealthDeclarationForm()
    return render(request, 'health_declaration/form.html',
                {'form': form})

class SwabTestResultDetailView(PermissionRequiredMixin, generic.DetailView):
    permission_required = 'health_declaration.view_swabtestresult'
    model = SwabTestResult

